<?
require '../../../../wp-config.php';

global $wpdb;
$wpdb->update(
	nasm_ia32_instruction,
	array(
		bits16 => null,
		bits32 => null,
		bits64 => null,
		quantrBits16 => null,
		quantrBits32 => null,
		quantrBits64 => null,
		pass => false,
		passDate => null
	),
	array('id'=> $_GET['id'])
);
?>
