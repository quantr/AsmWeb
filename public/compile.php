<?php
// to use temp file in php, systemd make /tmp private, so "vi /etc/systemd/system/multi-user.target.wants/apache2.service"
// , change PrivateTmp=false", then systemctl daemon-reload,
// run "systemctl show apache2 | grep PrivateTmp" for checking

if (!ctype_alpha($_POST['arch'])){
	die;
}

$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';

function getPath($tempFile)
{
	$metaData = stream_get_meta_data($tempFile);
	return $metaData['uri'];
}

$asmFile = tmpfile();
$binFile = tmpfile();
$tmpfile1 = tmpfile();
$tmpfile2 = tmpfile();
fwrite($asmFile, $_POST['code']);
if ($_POST['type'] == 'compile') {
	// echo "$jdk_home/bin/java -jar /home/gitlab-runner/assembler/assembler.jar -a " . $_POST['arch'] . " " . getPath($asmFile) . " -o " . getPath($binFile) . " 2>&1";
	//$compileOutput = shell_exec("ls");
	if ($_POST['arch'] == 'gas32') {
		$compileOutput = shell_exec("riscv64-linux-gnu-as -march=rv32imc " . getPath($asmFile) . " -o " . getPath($tmpfile1) . " 2>&1");
		$compileOutput .= shell_exec("riscv64-linux-gnu-ld -melf32lriscv " . getPath($tmpfile1) . " -T temp.ld -o " . getPath($tmpfile2) . " 2>&1");
		$compileOutput .= shell_exec("riscv64-linux-gnu-objcopy -O binary --only-section=.text " . getPath($tmpfile2) . " " . getPath($binFile) . " 2>&1");
	} else if ($_POST['arch'] == 'gas64') {
		$compileOutput .= shell_exec("riscv64-linux-gnu-as -march=rv64imc " . getPath($asmFile) . " -o " . getPath($tmpfile1) . " 2>&1");
		$compileOutput .= shell_exec("riscv64-linux-gnu-ld -melf64lriscv " . getPath($tmpfile1) . " -T temp.ld -o " . getPath($tmpfile2) . " 2>&1");
		$compileOutput .= shell_exec("riscv64-linux-gnu-objcopy -O binary --only-section=.text " . getPath($tmpfile2) . " " . getPath($binFile) . " 2>&1");
	} else {
		$compileOutput = shell_exec("$jdk_home/bin/java -jar /home/gitlab-runner/assembler/assembler.jar -a " . $_POST['arch'] . " " . getPath($asmFile) . " -o " . getPath($binFile) . " 2>&1");
	}
	// echo "$jdk_home/bin/java -jar /home/gitlab-runner/assembler/assembler.jar " . getPath($asmFile) . " -o " . getPath($binFile) . " 2>&1 ";
} else if ($_POST['type'] == 'version') {
	$compileOutput = shell_exec("$jdk_home/bin/java -jar /home/gitlab-runner/assembler/assembler.jar -v  2>&1");
}
// echo shell_exec("ls -l ".getPath($binFile));
fclose($asmFile);

if (filesize(getPath($binFile)) > 0) {
	$bin = fread($binFile, filesize(getPath($binFile)));
	// echo "size=".filesize(getPath($binFile));
	// echo "\n\n";
	// echo "strlen=".strlen($bin);
	// echo "\n\n";
	// echo "strpos=" . strpos(bin2hex($bin), bin2hex('ERROR'));
	// echo "\n\n";
}
echo $compileOutput;
// echo "\n\n";
fclose($binFile);

if (strpos(bin2hex($bin), bin2hex('ERROR')) === false || strpos(bin2hex($bin), bin2hex('ERROR')) == -1) {
	echo bin2hex($bin);
}
