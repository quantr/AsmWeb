<?php
require '../../../../wp-config.php';

function getPath($tempFile)
{
    $metaData = stream_get_meta_data($tempFile);
    return $metaData['uri'];
}

$tempFile=tmpfile();
fwrite($tempFile, hex2bin($_GET['bytesStr']));
if ($_GET['arch']=='rv32') {
    $compileOutput =  shell_exec("riscv64-unknown-elf-objdump -b binary -D  -m riscv:rv32 ".getPath($tempFile));
} else {
    $compileOutput =  shell_exec("riscv64-unknown-elf-objdump -b binary -D  -m riscv:rv64 ".getPath($tempFile));
}
fclose($tempFile);

echo $compileOutput;
