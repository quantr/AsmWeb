<?
require '../../../../wp-config.php';

//$currentUser = wp_get_current_user();
//$username=$currentUser->user_login;

$source="bits".$_GET['bits']."Temp.asm";

global $wpdb;
$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".nasm_ia32_instruction." where id=%d;", $_GET['id']), OBJECT);

$file = fopen(sys_get_temp_dir()."/".$source, "w");
if ($_GET['bits']==16){
	fwrite($file, "[bits 16]\n");
}else if ($_GET['bits']==32){
	fwrite($file, "[bits 32]\n");
}else{
	fwrite($file, "[bits 64]\n");
}
fwrite($file, $results[0]->code);
fclose($file);

exec("nasm ".sys_get_temp_dir()."/".$source." -o ".sys_get_temp_dir()."/bit16Temp.bin");

$hex = unpack("H*", file_get_contents(sys_get_temp_dir()."/bit16Temp.bin"));
$hex = current($hex);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, plugin_dir_url(__FILE__)."/save.php?id=".$_GET['id']."&bits=".$_GET['bits']."&data=".urlencode($hex));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response=curl_exec($ch);
echo "ok"
?>
