<?php
require plugin_dir_path(__FILE__) . '../../../../wp-config.php';
$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
if (!$conn) {
	echo 'Could not connect: ' . mysqli_error($conn);
}
$sql = "select distinct type from disasm order by type";
$stmt = mysqli_prepare($conn, $sql);
mysqli_stmt_execute($stmt);
$result = mysqli_stmt_get_result($stmt);
while ($row = mysqli_fetch_assoc($result)) {
	$disasmType .= "<option>" . $row['type'] . "</option>";
}
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__) ?>/fontawesome-free-5.10.1-web/css/all.css">
<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__) ?>/main.css">
<script>
	$.fn.andSelf = function() {
		return this.addBack.apply(this, arguments);
	}
	var version;
	$(function() {
		$('.text-logo').html('Quantr Research');
	});

	function viewOutput(path) {
		window.open('<?php echo plugin_dir_url(__FILE__) ?>/viewOutput.php?path=' +
			path);
	}
</script>
<?php
$currentUser = wp_get_current_user();
$username = $currentUser->user_login;

$samples = array(
	['ia32', 'aaa'],
	['ia32', 'adc al,0x12'],
	['ia32', 'adc ax,0x1234'],
	['ia32', 'adc byte [ax],0x12'],
	['ia32', 'adc word [ax],0x1234'],
	['ia32', 'add ax,bx'],
	['risc-v', 'auipc a0,0x18000'],
	['risc-v', 'addi a0,a0,0x0'],
	['risc-v', 'lui a2,0x40003'],
	['risc-v', 'c.andi a3,a3,2'],
	['risc-v', 'c.beqz a3,18'],
	['risc-v', 'jal ra,0x6'],
	['gas', 'xori x1,x2,0x30']
);
?>
<style>
	#qfLogo {
		width: 30px;
	}

	.nav-link {
		color: black;
	}

	#jobsTable th {
		text-align: center;
	}

	#jobsTable td {
		vertical-align: middle;
	}

	.badge {
		padding: 4px 5px;
		font-size: 12px;
		font-style: normal;
		font-weight: 400;
		display: inline-block;
	}

	.badge-primary {
		color: #fff;
		background-color: #007bff;
	}

	.ci-status.ci-success {
		color: #1aaa55;
		border-color: #1aaa55;
	}

	.ci-status {
		border: 1px solid;
		padding: 5px;
		border-radius: 5px;
	}

	.ci-status.ci-canceled,
	.ci-status.ci-disabled,
	.ci-status.ci-scheduled,
	.ci-status.ci-manual {
		color: #2e2e2e;
		border-color: #2e2e2e;
	}

	.ci-status.ci-failed {
		color: #db3b21;
		border-color: #db3b21;
	}

	.leftRightBorder {
		border-left: 1px solid #aaa;
		border-right: 1px solid #aaa;
		padding: 0px !important;
	}

	.field {
		color: #aaa;
		text-align: center;
	}

	.fieldValue {
		font-weight: bold;
		text-align: center;
	}

	.fieldHeader {
		font-size: 30px;
		font-weight: bold;
		text-align: center;
	}

	.highlight {
		background-color: pink;
	}

	.disasmCode {
		font-weight: bold;
		background-color: #d0d0d0;
	}

	#simulatorTable {
		width: auto;
	}

	.rightBorderSimulator {
		border-right: 3px solid black;
	}

	#decodeTable1 td {
		padding: 0px;
		vertical-align: middle;
	}

	#decodeTable2 td {
		padding: 0px;
		vertical-align: middle;
	}

	.disasmBorderLeft {
		border-left: 2px solid #dfdfdf;
	}
</style>
<!-- <div class="topBanner">
    <div class="container">
        <h1>Assembler project portal</h1>
        <p class="lead">
            Getting started
        </p>
    </div>
</div> -->
<div class="container-fuild" style="margin: 5px; margin-top:10px;">
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#gitlabPipeline" role="tab" aria-controls="gitlabPipeline" aria-selected="true">GitLab Pipeline</a>
		</li>
		<!-- <li class="nav-item">
            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Test Bench</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="online-test-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Online test</a>
        </li> -->
		<li class="nav-item">
			<a class="nav-link" id="pipelineSummaryTab" data-toggle="tab" href="#pipelineSummary" role="tab" aria-controls="pipelineSummary" aria-selected="false">Summary</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="pipelineResultTab" data-toggle="tab" href="#pipelineResult" role="tab" aria-controls="pipelineResult" aria-selected="false" onclick="window.location.hash='pipelineResult';">Result</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="onlineAssemblerTab" data-toggle="tab" href="#onlineAssembler" role="tab" aria-controls="onlineAssembler" aria-selected="false">Assembler</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="onlineDisassemblerTab" data-toggle="tab" href="#onlineDisassembler" role="tab" aria-controls="onlineDisassembler" aria-selected="false">Disassembler</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="riscvDecodeTab" data-toggle="tab" href="#riscvDecode" role="tab" aria-controls="riscvDecode" aria-selected="false">RISC-V decode</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="testDisassemblerab" data-toggle="tab" href="#testDisassembler" role="tab" aria-controls="testDisassembler" aria-selected="false">Test Disassembler</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="testSimulatorTab" data-toggle="tab" href="#testSimulator" role="tab" aria-controls="testSimulator" aria-selected="false">Test Simulator</a>
		</li>
	</ul>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="gitlabPipeline" role="tabpanel" aria-labelledby="gitlab-pipeline-tab">
			<table class="table" id="jobsTable">
				<thead class="thead-light">
					<tr>
						<th>Status</th>
						<th>Job</th>
						<th>Pipeline</th>
						<th class="d-none d-sm-table-cell">Stage</th>
						<th class="d-none d-sm-table-cell">Commit Msg</th>
						<th class="d-none d-sm-table-cell">Timing</th>
						<th class="d-none d-sm-table-cell">Output</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="7" style="text-align: center;">
							<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="pipelineSummary" role="tabpanel" aria-labelledby="pipelineSummaryTab">
			<div class="row d-none d-sm-block" style="padding: 5px;">
				<div class="col" style="text-align: center;">
					<div class="form-check-inline">
						<input class="form-check-input" type="radio" name="arch" id="arch" onclick="reloadPipelineSummaryResultTable();" value="RISC-V" checked> RISC-V &nbsp;
						<input class="form-check-input" type="radio" name="arch" id="arch" onclick="reloadPipelineSummaryResultTable();" value="IA32"> IA32 &nbsp;
						<select id="pipelineSummaryResultCIJOBIDDropdown" class="form-control" style="width: 400px; margin-right: 20px;" onchange="reloadPipelineSummaryResultTable();">
							<?php
							$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
							if (!$conn) {
								echo 'Could not connect: ' . mysqli_error($conn);
							}
							$result = mysqli_query($conn, "select * from job order by datetime desc;");
							while ($row = mysqli_fetch_assoc($result)) {
								echo "<option value='" . $row['CI_JOB_ID'] . "'>" . $row['datetime'] . ' - ' . $row['CI_JOB_ID'] . ' - ' . $row['GITLAB_USER_NAME'] . "</option>";
							}
							mysqli_close($conn);
							?>
						</select>
						<input type="checkbox" class="form-check-input" id="pipelineSummaryErrorOnly" onchange="reloadPipelineSummaryResultTable();" /> Show error only
					</div>
				</div>
			</div>
			<div id="pipelineSummaryResultIA32"></div>
			<div id="pipelineSummaryResultRISCV"></div>
		</div>
		<div class="tab-pane fade" id="pipelineResult" role="tabpanel" aria-labelledby="pipelineResultTab">
			<div class="container-fuild">
				<div class="row" style="padding: 5px;">
					<div class="col-12 form-check form-check-inline justify-content-center">
						<select id="pipelineResultCIJOBIDDropdown" class="form-control" style="width: 400px; margin-right: 20px;" onchange="reloadPipelineResultTable(1);">
							<?php
							$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
							if (!$conn) {
								echo 'Could not connect: ' . mysqli_error($conn);
							}
							$result = mysqli_query($conn, "select * from job order by datetime desc;");
							while ($row = mysqli_fetch_assoc($result)) {
								echo "<option value='" . $row['CI_JOB_ID'] . "'>" . $row['datetime'] . ' - ' . $row['CI_JOB_ID'] . ' - ' . $row['GITLAB_USER_NAME'] . "</option>";
							}
							mysqli_close($conn);
							?>
						</select>
					</div>
				</div>
				<div class="row" style="padding: 5px;">
					<div class="col-12 form-check form-check-inline justify-content-center">
						<input class="form-check-input" type="radio" name="arch2" id="arch2_1" onclick="loadInstructionDropdown(this.value);" value="RISC-V" checked> <label for="arch2_1" style="margin-bottom: 0px;">RISC-V</label>&nbsp;
						<input class="form-check-input" type="radio" name="arch2" id="arch2_2" onclick="loadInstructionDropdown(this.value);" value="IA32"> <label for="arch2_2" style="margin-bottom: 0px;">IA32</label>&nbsp;
						<select id="pipelineResultInstructionDropdown" class="form-control" style="width: 170px; margin-right: 20px;" onchange="reloadPipelineResultTable(1);">
							<option value="">All</option>
						</select>
						<input class="form-check-input" type="radio" name="pipelineResultTableCheckBox" id="pipelineResultTableCheckBox1" value="all" checked onclick="reloadPipelineResultTable(1);" /> <label for="pipelineResultTableCheckBox1" style="margin-bottom: 0px;">All</label>&nbsp;
						<input class="form-check-input" type="radio" name="pipelineResultTableCheckBox" id="pipelineResultTableCheckBox2" value="error" onclick="reloadPipelineResultTable(1);" />
						<label for="pipelineResultTableCheckBox2" style="margin-bottom: 0px;">Error</label>&nbsp;
						<input class="form-check-input" type="radio" name="pipelineResultTableCheckBox" id="pipelineResultTableCheckBox3" value="mis-encoded" onclick="reloadPipelineResultTable(1);" /> <label for="pipelineResultTableCheckBox3" style="margin-bottom: 0px;">Mis-encoded</label>&nbsp;
						&nbsp;
						<input class="form-control" type="text" name="searchPipelineResultTable" id="searchPipelineResultTable" placeholder="Search" style="width: 200px; margin-bottom: 0px;">
					</div>
					<nav id="pipelineResultTablePaging" aria-label="Page navigation example"></nav>
				</div>
			</div>
			<table class="table" id="pipelineResultTable">
				<thead class="thead-light">
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="onlineAssembler" role="tabpanel" aria-labelledby="onlineAssembler-tab">
			<!-- <div class="alert alert-danger" role="alert">
				Under develop
			</div> -->
			<div class="container-fuild">
				<div class="row">
					<div class="col-3 d-none d-md-block">
						Samples<br />
						<ul class="list-group">
							<?php for ($x = 0; $x < count($samples); $x++) { ?>
								<li class="list-group-item" style="cursor: pointer;" onclick="pasteCode('<?= $samples[$x][0] ?>','<?= $samples[$x][1] ?>');">
									<span class="sample_<?= $samples[$x][0] ?>"><?= $samples[$x][0] ?></span> <?= $samples[$x][1] ?>
								</li>
							<?php } ?>
						</ul>
					</div>
					<div class="col-9">
						<textarea id="asmCode" class="form-control" placeholder="Enter your asm code here" style="margin-top: 10px; margin-bottom: 10px; height:400px;"></textarea>
						<button type="button" class="btn btn-primary" onclick="compile($('#asmCode').val(), 'compile', 'ia32');">Compile IA32</button>
						<button type="button" class="btn btn-primary" onclick="compile($('#asmCode').val(), 'compile', 'rv32');">Compile RISC-V 32 bits</button>
						<button type="button" class="btn btn-primary" onclick="compile($('#asmCode').val(), 'compile', 'rv64');">Compile RISC-V 64 bits</button>
						<button type="button" class="btn btn-primary" onclick="compile($('#asmCode').val(), 'compile', 'gas32');">Compile Gas 32 bits</button>
						<button type="button" class="btn btn-primary" onclick="compile($('#asmCode').val(), 'compile', 'gas64');">Compile Gas 64 bits</button>
						<button type="button" class="btn btn-primary" onclick="compile($('#asmCode').val(), 'version');">Version</button>
						<textarea id="asmOutputCode" class="form-control" placeholder="Assembler output goes here" style="margin-top: 10px; height:400px; font-family: monospace;"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="onlineDisassembler" role="tabpanel" aria-labelledby="onlineDisassembler-tab">
			<?php include('disassembler.php'); ?>
		</div>
		<div class="tab-pane fade" id="riscvDecode" role="tabpanel" aria-labelledby="riscvDecodeTab">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-md-6 col-sm-12 my-1">
						Reverse the bytes to input, first byte is bit 0-7
						<div class="input-group">
							<div class="form-outline">
								<input id="decodeRiscVField" type="text" class="form-control" value="b3011100" onkeyup="decodeRiscV(this.value)" />
							</div>
							<button type="button" class="btn btn-primary" onclick="revertDecodeBytes(); decodeRiscV($('#decodeRiscVField').val());">
								<i class="fas fa-exchange-alt"></i>
							</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div id="riscvDecodeDiv"></div>
					</div>
				</div>
				<div class="row">
					<div class="col" style="padding: 0px;">
						<table id="decodeTable1" class="clickToSelect table" style="table-layout: fixed;">
							<tr>
								<td></td>
								<?php for ($x = 31; $x >= 0; $x--) { ?>
									<td <?= ($x <= 7 || ($x >= 16 && $x < 24)) ? 'style="background-color: #eee; text-align: center;"' : 'style="text-align: center;"' ?>><?= $x ?>
									</td>
								<?php } ?>
							</tr>
							<tr>
								<td class="fieldHeader">R</td>
								<td class="leftRightBorder" colspan="7">
									<div id="R_funct7" class="fieldValue"></div>
									<div class="field">funct7</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="R_rs2" class="fieldValue"></div>
									<div class="field">rs2</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="R_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="R_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="R_rd" class="fieldValue"></div>
									<div class="field">rd</div>
								</td>
								<td class="leftRightBorder" colspan="7">
									<div id="R_opcode" class="fieldValue"></div>
									<div class="field">opcode</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">I</td>
								<td class="leftRightBorder" colspan="12">
									<div id="I_imm" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="I_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="I_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="I_rd" class="fieldValue"></div>
									<div class="field">rd</div>
								</td>
								<td class="leftRightBorder" colspan="7">
									<div id="I_opcode" class="fieldValue"></div>
									<div class="field">opcode</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">S</td>
								<td class="leftRightBorder" colspan="7">
									<div id="S_imm11_5" class="fieldValue"></div>
									<div class="field">imm[11:5]</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="S_rs2" class="fieldValue"></div>
									<div class="field">rs2</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="S_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="S_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="S_imm4_0" class="fieldValue"></div>
									<div class="field">imm[4:0]</div>
								</td>
								<td class="leftRightBorder" colspan="7">
									<div id="S_opcode" class="fieldValue"></div>
									<div class="field">opcode</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">B</td>
								<td class="leftRightBorder" colspan="7">
									<div id="B_imm12_10_5" class="fieldValue"></div>
									<div class="field">imm[12|10:5]</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="B_rs2" class="fieldValue"></div>
									<div class="field">rs2</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="B_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="B_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="B_imm4_1_11" class="fieldValue"></div>
									<div class="field">imm[4:1|11]</div>
								</td>
								<td class="leftRightBorder" colspan="7">
									<div id="B_opcode" class="fieldValue"></div>
									<div class="field">opcode</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">U</td>
								<td class="leftRightBorder" colspan="20">
									<div id="U_imm31_12" class="fieldValue"></div>
									<div class="field">imm[31:12]</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="U_rd" class="fieldValue"></div>
									<div class="field">rd</div>
								</td>
								<td class="leftRightBorder" colspan="7">
									<div id="U_opcode" class="fieldValue"></div>
									<div class="field">opcode</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">J</td>
								<td class="leftRightBorder" colspan="20">
									<div id="J_imm20_10_1_11_19_12" class="fieldValue"></div>
									<div class="field">imm[20|10:1|11|19:12]</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="J_rd" class="fieldValue"></div>
									<div class="field">rd</div>
								</td>
								<td class="leftRightBorder" colspan="7">
									<div id="J_opcode" class="fieldValue"></div>
									<div class="field">opcode</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col" style="background-color: #bcbcbc;">&nbsp;</div>
				</div>
				<div class="row">
					<div class="col" style="padding: 0px;">
						<table id="decodeTable2" class="clickToSelect table" style="table-layout: fixed;">
							<tr>
								<td></td>
								<?php for ($x = 15; $x >= 0; $x--) { ?>
									<td <?= ($x <= 7 || ($x >= 16 && $x < 24)) ? 'style="background-color: #eee; text-align: center;"' : 'style="text-align: center;"' ?>><?= $x ?>
									</td>
								<?php } ?>
							</tr>
							<tr>
								<td class="fieldHeader">CR</td>
								<td class="leftRightBorder" colspan="4">
									<div id="CR_funct4" class="fieldValue"></div>
									<div class="field">funct4</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="CR_rd_rs1" class="fieldValue"></div>
									<div class="field">rd/rs1</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="CR_rs2" class="fieldValue"></div>
									<div class="field">rs2</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CR_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CI</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CI_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="1">
									<div id="CI_imm" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="CI_rd_rs1" class="fieldValue"></div>
									<div class="field">rd/rs1</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="CI_imm_2_6" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CI_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CSS</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CSS_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="6">
									<div id="CSS_imm" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="CSS_rs2" class="fieldValue"></div>
									<div class="field">rs2</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CSS_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CIW</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CIW_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="8">
									<div id="CIW_imm" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CIW_rd" class="fieldValue"></div>
									<div class="field">rd</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CIW_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CL</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CL_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CL_imm" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CL_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CL_imm_5_6" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CL_rd" class="fieldValue"></div>
									<div class="field">rd</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CL_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CS</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CS_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CS_imm" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CS_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CS_imm_5_6" class="fieldValue"></div>
									<div class="field">imm</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CS_rs2" class="fieldValue"></div>
									<div class="field">rs2</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CS_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CB</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CB_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CB_offset" class="fieldValue"></div>
									<div class="field">offset</div>
								</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CB_rs1" class="fieldValue"></div>
									<div class="field">rs1</div>
								</td>
								<td class="leftRightBorder" colspan="5">
									<div id="CB_offset_2_6" class="fieldValue"></div>
									<div class="field">offset</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CB_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
							<tr>
								<td class="fieldHeader">CJ</td>
								<td class="leftRightBorder" colspan="3">
									<div id="CJ_funct3" class="fieldValue"></div>
									<div class="field">funct3</div>
								</td>
								<td class="leftRightBorder" colspan="11">
									<div id="CJ_jump_target" class="fieldValue"></div>
									<div class="field">jump target</div>
								</td>
								<td class="leftRightBorder" colspan="2">
									<div id="CJ_op" class="fieldValue"></div>
									<div class="field">op</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="testDisassembler" role="tabpanel" aria-labelledby="testDisassemblerTab">
			<div class="container-fuild">
				<div class="row" style="padding: 5px;">
					<div class="col-12 form-check form-check-inline justify-content-center">
						<select id="testDisassemblerCIJOBIDDropdown" class="form-control" style="width: 400px; margin-right: 20px;" onchange="loadDisassemblerResult_real();">
							<?php
							$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
							if (!$conn) {
								echo 'Could not connect: ' . mysqli_error($conn);
							}
							$result = mysqli_query($conn, "select * from job order by datetime desc;");
							while ($row = mysqli_fetch_assoc($result)) {
								echo "<option value='" . $row['CI_JOB_ID'] . "'>" . $row['datetime'] . ' - ' . $row['CI_JOB_ID'] . ' - ' . $row['GITLAB_USER_NAME'] . "</option>";
							}
							mysqli_close($conn);
							?>
						</select>
						Type
						<select id="testDisassemblerType" class="form-control" style="width: 100px; margin-left: 20px; margin-right: 20px;" onchange="loadDisassemblerResult_real();">
							<option value="">All</option>
							<?= $disasmType ?>
						</select>
						Page
						<select id="testDisassemblerPageDropdown" class="form-control" style="width: 100px; margin-left: 20px; margin-right: 20px;" onchange="loadDisassemblerResult_real();">
						</select>
						Search
						<input id="testDisassemblerSearch" class="form-control" type="search" style="width: 100px; margin-left: 20px; margin-right: 20px;" onKeyUp="loadDisassemblerResult();" />
						Distance
						<select id="testDisassemblerDistance" class="form-control" style="width: 100px; margin-left: 20px; margin-right: 20px;" onchange="loadDisassemblerResult_real();">
							<option value="0">0</option>
							<option value="10">&lt; 10</option>
							<option value="50">&lt; 50</option>
							<option value="100">&lt; 100</option>
							<option value="500" selected>&lt; 500</option>
						</select>
						<span id="testDisassemblerCount" />
					</div>
				</div>
				<div class="row" style="padding: 5px;">
					<div class="col-12 form-check form-check-inline justify-content-center">
						<div id="testDisassemblerResult"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="testSimulator" role="tabpanel" aria-labelledby="testSimulatorTab">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 form-check form-check-inline justify-content-center">
						<select id="testSimulatorDropdown" class="form-control" onchange="loadSimulationRecord($('#testSimulatorDropdown').val()); loadSimulatorBinary($('#testSimulatorDropdown').val());" style="width: 220px;">
							<?php
							$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
							if (!$conn) {
								echo 'Could not connect: ' . mysqli_error($conn);
							}
							$result = mysqli_query($conn, "select distinct guid,date from cpuRecorder order by date desc;");
							while ($row = mysqli_fetch_assoc($result)) {
								echo "<option value='" . $row['guid'] . "'>" . $row['date'] . "</option>";
							}
							mysqli_close($conn);
							?>
						</select>
						<input type="checkbox" id="disasmMode" class="form-check-input ml-2" checked onchange="loadSimulationRecord($('#testSimulatorDropdown').val()); loadSimulatorBinary($('#testSimulatorDropdown').val());" />
						<div style="width: 200px;"><label for="disasmMode" style="margin-bottom: 0px;">Simplified Table</label></div>
						<input type="file" id="fileUploaderSimulatorBinary" stlye="width: 200px;" onchange="loadSimulatorBinary($('#testSimulatorDropdown').val());" />
						<button type="button" onclick="loadSimulatorBinary($('#testSimulatorDropdown').val());" class="btn btn-secondary mb-2 mt-2">Load binary file</button>
						&nbsp;
						<i class="fas fa-check-circle" style="color: #00cc00;"></i>&nbsp;<span id="simulatorCorrect">0</span>
						&nbsp;
						<i class="fas fa-times-circle" style="color: #cc0000;"></i>&nbsp;<span id="simulatorWrong">0</span>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-12" id="cpuRecorderTableLeft" style="overflow: scroll;">
						<table id="cpuRecorderTable" class="table table-striped">
						</table>
					</div>
					<div class="col-6" id="simulatorTableDiv" style="overflow: scroll; display:none">
						<table id="simulatorTable" class="table table-striped">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="modal" id="addInstructionModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add instruction</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col">
						<input type="radio" name="instructionType" value="ia32" checked> IA32
						<input type="radio" name="instructionType" value="arm"> Arm
					</div>
				</div>
				<div class="separator1"></div>
				<div class="row">
					<div class="col">
						<input type="text" id="code" class="form-control" placeholder="Nasm code" autofocus />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="addInstruction();">Save</button>
			</div>
		</div>
	</div>
</div> -->
<script>
	var shouldRefresh = true;

	function pasteCode(type, code) {
		if (type == 'ia32') {
			$('#asmCode').val('(bits 32){\n\t' + code + '\n}');
		} else if (type == 'gas') {
			$('#asmCode').val('.section .text\n.globl _start\n_start:\n\t' + code + '\n\t');
		} else {
			$('#asmCode').val(code);
		}
	}

	function loadSimulatorBinary(guid) {
		// alert($('#'));

		$('#simulatorTable').html(`
		<tr>
			<td style="text-align: center;">
				<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
			</td>
		</tr>
	`);

		var file_data = $('#fileUploaderSimulatorBinary').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/uploadSimulatorBinary.php?mode=' + $('#disasmMode').prop('checked') + '&guid=' + guid,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(data) {
				$('#cpuRecorderTableLeft').removeClass("col-12");
				$('#cpuRecorderTableLeft').addClass("col-6");

				$('#simulatorTable').html(data);
				$('#simulatorTableDiv').css('display', '');
			}
		});
	}

	function loadSimulationRecord(guid) {
		$('#cpuRecorderTable').html(`
		<tr>
			<td style="text-align: center;">
				<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
			</td>
		</tr>
	`);
		$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadCpuRecorderTable.php?guid=' + guid + '&mode=' + $('#disasmMode').prop('checked'),
			function(data) {
				$('#cpuRecorderTable').html(data);
			});
	}

	function pad(num, size) {
		var s = "0000000000000000" + num;
		return s.substr(s.length - size);
	}

	// function reloadTable() {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadTable.php', function(data) {
	// 		$('#instructionTable tbody').html(data);
	// 	});
	// }

	function reloadJobsTable() {
		$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadJobsTable.php',
			function(data) {
				$('#jobsTable tbody').html(data);
			});
	}

	function reloadPipelineResultTable(pageNo) {
		if (!shouldRefresh) {
			return;
		}
		var CI_JOB_ID = $('#pipelineResultCIJOBIDDropdown').val();
		$('#pipelineResultTable tbody').html(`
			<tr>
				<td colspan="7" style="text-align: center;">
					<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
				</td>
			</tr>
		`);
		var arch = $('input[name="arch2"]:checked').val();
		if (arch == 'IA32') {
			$('#pipelineResultTable thead').html(`
				<tr>
					<th align="center">ID</th>
					<th align="center">Row No</th>
					<th align="left">Code</th>
					<th>Quantr 16</th>
					<th>Nasm 16</th>
					<th>Quantr 32</th>
					<th>Nasm 32</th>
					<th>Quantr 64</th>
					<th>Nasm 64</th>
				</tr>
			`);
		} else if (arch == 'RISC-V') {
			$('#pipelineResultTable thead').html(`
				<tr>
					<th align="center">ID</th>
					<th align="center">Row No</th>
					<th align="left">Code</th>
					<th>Quantr 32</th>
					<th>Quantr 32 Error</th>
					<th>Gas 32 Code</th>
					<th>Gas 32</th>
					<th>Gas 32 Error</th>
					<th>Quantr 64</th>
					<th>Quantr 64 Error</th>
					<th>Gas 64 Code</th>
					<th>Gas 64</th>
					<th>Gas 64 Error</th>
				</tr>
			`);
		}
		$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadPipelineResultTable.php?type=table&arch=' +
			arch + '&CI_JOB_ID=' + CI_JOB_ID + '&pageNo=' + pageNo + '&selectType=' + $(
				'input[name="pipelineResultTableCheckBox"]:checked').val() + '&instruction=' + $(
				'#pipelineResultInstructionDropdown').val() + '&search=' + $('#searchPipelineResultTable').val(),
			function(data) {
				$('#pipelineResultTable tbody').html(data);
			});
		$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadPipelineResultTable.php?type=paging&arch=' +
			arch + '&CI_JOB_ID=' + CI_JOB_ID + '&pageNo=' + pageNo + '&selectType=' + $(
				'input[name="pipelineResultTableCheckBox"]:checked').val() + '&instruction=' + $(
				'#pipelineResultInstructionDropdown').val() + '&search=' + $('#searchPipelineResultTable').val(),
			function(data) {
				$('#pipelineResultTablePaging').html(data);
			});
	}

	function reloadPipelineSummaryResultTable() {
		reloadPipelineSummaryTable();
	}

	function reloadPipelineSummaryTable() {
		$('#pipelineSummaryResultIA32').html(null);
		$('#pipelineSummaryResultRISCV').html(null);

		var CI_JOB_ID = $('#pipelineSummaryResultCIJOBIDDropdown').val();
		if ($('#arch:checked').val() == 'IA32') {
			$('#pipelineSummaryResultIA32').html(
				'<div style="text-align: center"><img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" /></div>'
			);
			$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadPipelineSummaryTable.php?arch=IA32&CI_JOB_ID=' +
				CI_JOB_ID + '&showErrorOnly=' + $('#pipelineSummaryErrorOnly').prop('checked'),
				function(data) {
					$('#pipelineSummaryResultIA32').html(data);
				});
		} else if ($('#arch:checked').val() == 'RISC-V') {
			$('#pipelineSummaryResultRISCV').html(
				'<div style="text-align: center"><img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" /></div>'
			);
			$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadPipelineSummaryTable.php?arch=RISC-V&CI_JOB_ID=' +
				CI_JOB_ID + '&showErrorOnly=' + $('#pipelineSummaryErrorOnly').prop('checked'),
				function(data) {
					$('#pipelineSummaryResultRISCV').html(data);
				});
		}
	}

	// function reloadFullTestTable() {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadFullTestTable.php', function(data) {
	// 		$('#fullTestTable tbody').html(data);
	// 	});
	// }

	// function reloadFullTestRecordTable() {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/reloadFullTestRecordTable.php', {
	// 		async: true
	// 	}, function(data) {
	// 		$('#fullTestRecordTable tbody').html(data);
	// 	});
	// }

	// function addInstruction() {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/addInstruction.php?code=' +
	// 		$('#code').val(),
	// 		function(data) {
	// 			$('#addInstructionModal').modal('hide');
	// 			reloadTable();
	// 		});
	// }

	// function addAllInstructions() {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/addAllInstructions.php?code=' +
	// 		$('#code').val(),
	// 		function(data) {
	// 			reloadTable();
	// 		});
	// }

	// function clearInstruction(id) {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/clearInstruction.php?id=' +
	// 		id,
	// 		function(data) {
	// 			$('#addInstructionModal').modal('hide');
	// 			reloadTable();
	// 		});
	// }

	// function deleteInstruction(id) {
	// 	$.get('<?php echo plugin_dir_url(__FILE__) ?>/deleteInstruction.php?id=' +
	// 		id,
	// 		function(data) {
	// 			$('#addInstructionModal').modal('hide');
	// 			reloadTable();
	// 		});
	// }

	function execNasm(id) {
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execNasm.php?id=' +
				id + '&bits=16',
			success: function(data) {},
			async: false
		});
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execNasm.php?id=' +
				id + '&bits=32',
			success: function(data) {},
			async: false
		});
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execNasm.php?id=' +
				id + '&bits=64',
			success: function(data) {},
			async: false
		});

		// compile by quantr Assembler
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execQuantrAssembler.php?id=' +
				id + '&bits=16',
			success: function(data) {},
			async: false
		});
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execQuantrAssembler.php?id=' +
				id + '&bits=32',
			success: function(data) {},
			async: false
		});
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execQuantrAssembler.php?id=' +
				id + '&bits=64',
			success: function(data) {},
			async: false
		});

		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/save.php?id=' +
				id + '&bits=64&type=pass',
			success: function(data) {},
			async: false
		});

		/*if ($('#bit16_'+id).html()==$('#bit16_quantr_assembler_'+id).html()){
		    $.ajax({
		        url: '<?php echo plugin_dir_url(__FILE__) ?>
		/save.php?id='+id+'&bits=64&type=pass&data=1',
		success: function(data) {},
			async :false
	});
	} else {
		//$('#bit64_pass_'+id).html('no');
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/save.php?id=' +
				id + '&bits=64&type=pass&data=0',
			success: function(data) {},
			async: false
		});
	}*/
		//reloadTable();
	}

	$('#searchPipelineResultTable').on('keypress', function(e) {
		if (e.which == 13) {
			reloadPipelineResultTable(1);
		}
	});

	function runFullTest() {
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/fulltest.php',
			success: function(data) {},
			async: false
		});
	}

	function compile(asmCode, type, arch) {
		$('#asmOutputCode').val(null);
		$.ajax({
			type: "POST",
			url: '<?php echo plugin_dir_url(__FILE__) ?>/compile.php',
			data: {
				code: asmCode,
				type: type,
				arch: arch
			},
			success: function(data) {
				$('#asmOutputCode').val(data);
			},
			async: false
		});
	}

	function loadInstructionDropdown(arch) {
		if (arch == 'IA32') {
			$('#pipelineResultInstructionDropdown').html(null);
			$('#pipelineResultInstructionDropdown').append('<option value="">ALL</option>');

			<?php
			$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
			if (!$conn) {
				echo "alert('Could not connect: " . mysqli_error($conn) . "');";
			}
			$result = mysqli_query($conn, "select distinct lower(SUBSTRING_INDEX(code, ' ', 1)) as instruction, SUBSTRING_INDEX(code, ' ', 1) as instruction2 from test where CI_JOB_ID=(select CI_JOB_ID from test order by datetime desc limit 0,1) order by instruction;");
			while ($row = mysqli_fetch_assoc($result)) {
				echo "$('#pipelineResultInstructionDropdown').append(\"'<option value='" . $row['instruction2'] . "'>" . $row['instruction'] . "</option>\");";
			}
			mysqli_close($conn);
			?>
		} else if (arch == 'RISC-V') {
			$('#pipelineResultInstructionDropdown').html(null);
			$('#pipelineResultInstructionDropdown').append('<option value="">ALL</option>');

			<?php
			$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
			if (!$conn) {
				echo "alert('Could not connect: " . mysqli_error($conn) . "');";
			}
			$result = mysqli_query($conn, "select distinct lower(SUBSTRING_INDEX(quantrCode, ' ', 1)) as instruction, SUBSTRING_INDEX(quantrCode, ' ', 1) as instruction2 from test_riscv where CI_JOB_ID=(select CI_JOB_ID from test_riscv order by datetime desc limit 0,1) order by instruction;");
			while ($row = mysqli_fetch_assoc($result)) {
				echo "$('#pipelineResultInstructionDropdown').append(\"'<option value='" . $row['instruction2'] . "'>" . $row['instruction'] . "</option>\");";
			}
			mysqli_close($conn);
			?>
		}
		reloadPipelineResultTable(1);
	}

	function beAFuckingTable(str) {
		r = "<table style=\"width: 100%;\"><tr>";
		for (x = 0; x < str.length; x++) {
			r += "<td style=\"border: 0px; text-align: center; font-weight: bold;\">" + str.charAt(x) + "</td>";
		}
		r += "</tr></table>";
		return r;
	}

	function decodeRiscV(value) {
		value = value.replace(/ /g, '');
		value = parseInt(value, 16);
		// if (value <= 0xffff) {
		// 	byte0 = value >> 8 & 0xff;
		// 	byte1 = value >> 0 & 0xff
		// } else {
		// 	byte0 = value >> 24 & 0xff;
		// 	byte1 = value >> 16 & 0xff
		// 	byte2 = value >> 8 & 0xff;
		// 	byte3 = value >> 0 & 0xff;
		// }

		if (value <= 0xffff) {
			byte0 = value >> 0 & 0xff;
			byte1 = value >> 8 & 0xff
		} else {
			byte0 = value >> 0 & 0xff;
			byte1 = value >> 8 & 0xff
			byte2 = value >> 16 & 0xff;
			byte3 = value >> 24 & 0xff;
		}

		// R
		v = (byte0 >> 1);
		$('#R_funct7').html(addValue(v, 7));
		v = ((byte0 & 1) << 4) | (byte1 >> 4);
		$('#R_rs2').html(addValue(v, 5));
		v = ((byte1 & 0xf) << 1) | (byte2 >> 7);
		$('#R_rs1').html(addValue(v, 5));
		v = (byte2 >> 4) & 0x7;
		$('#R_funct3').html(addValue(v, 3));
		v = ((byte2 & 0xf)) << 1 | (byte3 >> 7);
		$('#R_rd').html(addValue(v, 5));
		v = (byte3 & 0x7f);
		$('#R_opcode').html(addValue(v, 7));

		// I
		v = (byte0 << 4) | ((byte1 >> 4) & 0xf);
		$('#I_imm').html(addValue(v, 12));
		v = ((byte1 & 0xf) << 1) | (byte2 >> 7);
		$('#I_rs1').html(addValue(v, 5));
		v = (byte2 >> 4) & 0x7;
		$('#I_funct3').html(addValue(v, 3));
		v = ((byte2 & 0xf)) << 1 | (byte3 >> 7);
		$('#I_rd').html(addValue(v, 5));
		v = (byte3 & 0x7f);
		$('#I_opcode').html(addValue(v, 7));

		// S
		v = (byte0 >> 1);
		$('#S_imm11_5').html(addValue(v, 7));
		v = ((byte0 & 1) << 4) | (byte1 >> 4);
		$('#S_rs2').html(addValue(v, 5));
		v = ((byte1 & 0xf) << 1) | (byte2 >> 7);
		$('#S_rs1').html(addValue(v, 5));
		v = (byte2 >> 4) & 0x7;
		$('#S_funct3').html(addValue(v, 3));
		v = ((byte2 & 0xf)) << 1 | (byte3 >> 7);
		$('#S_imm4_0').html(addValue(v, 5));
		v = (byte3 & 0x7f);
		$('#S_opcode').html(addValue(v, 7));

		// B
		v = (byte0 >> 1);
		$('#B_imm12_10_5').html(addValue(v, 7));
		v = ((byte0 & 1) << 4) | (byte1 >> 4);
		$('#B_rs2').html(addValue(v, 5));
		v = ((byte1 & 0xf) << 1) | (byte2 >> 7);
		$('#B_rs1').html(addValue(v, 5));
		v = (byte2 >> 4) & 0x7;
		$('#B_funct3').html(addValue(v, 3));
		v = ((byte2 & 0xf)) << 1 | (byte3 >> 7);
		$('#B_imm4_1_11').html(addValue(v, 5));
		v = (byte3 & 0x7f);
		$('#B_opcode').html(addValue(v, 7));

		// U
		v = (byte0 << 12) | (byte1 << 4) | (byte2 >> 4);
		$('#U_imm31_12').html(addValue(v, 20));
		v = ((byte2 & 0xf)) << 1 | (byte3 >> 7);
		$('#U_rd').html(addValue(v, 5));
		v = (byte3 & 0x7f);
		$('#U_opcode').html(addValue(v, 7));

		// U
		v = (byte0 << 12) | (byte1 << 4) | (byte2 >> 4);
		$('#J_imm20_10_1_11_19_12').html(addValue(v, 20));
		v = ((byte2 & 0xf)) << 1 | (byte3 >> 7);
		$('#J_rd').html(addValue(v, 5));
		v = (byte3 & 0x7f);
		$('#J_opcode').html(addValue(v, 7));

		// CR
		$('#CR_funct4').html(addValue(byte0 >> 4, 4));
		$('#CR_rd_rs1').html(addValue(((byte0 & 0xf) << 1) | (byte1 >> 7), 5));
		$('#CR_rs2').html(addValue(byte1 >> 2 & 0b11111, 5));
		$('#CR_op').html(addValue(byte1 & 0b11, 2));

		// CI
		$('#CI_funct3').html(addValue(byte0 >> 5, 3));
		$('#CI_imm').html(addValue((byte0 >> 4) & 1, 1));
		$('#CI_rd_rs1').html(addValue(((byte0 & 0xf) << 1) | (byte1 >> 7), 5));
		$('#CI_imm_2_6').html(addValue(byte1 >> 2 & 0b11111, 5));
		$('#CI_op').html(addValue(byte1 & 0b11, 2));

		// CSS
		$('#CSS_funct3').html(addValue(byte0 >> 5, 3));
		$('#CSS_imm').html(addValue(((byte0 & 0b11111) << 1) | (byte1 >> 7), 6));
		$('#CSS_rs2').html(addValue(byte1 >> 2 & 0b11111, 5));
		$('#CSS_op').html(addValue(byte1 & 0b11, 2));

		// CIW
		$('#CIW_funct3').html(addValue(byte0 >> 5, 3));
		$('#CIW_imm').html(addValue(((byte0 & 0b11111) << 3) | (byte1 >> 5), 8));
		$('#CIW_rd').html(addValue(byte1 >> 2 & 0b111, 3));
		$('#CIW_op').html(addValue(byte1 & 0b11, 2));

		// CL
		$('#CL_funct3').html(addValue(byte0 >> 5, 3));
		$('#CL_imm').html(addValue((byte0 >> 2), 3));
		$('#CL_rs1').html(addValue(((byte0 & 0b11) << 1) | (byte1 >> 7), 3));
		$('#CL_imm_5_6').html(addValue(((byte1 >> 5) & 0b11), 2));
		$('#CL_rd').html(addValue(byte1 >> 2 & 0b111, 3));
		$('#CL_op').html(addValue(byte1 & 0b11, 2));

		// CS
		$('#CS_funct3').html(addValue(byte0 >> 5, 3));
		$('#CS_imm').html(addValue((byte0 >> 2), 3));
		$('#CS_rs1').html(addValue(((byte0 & 0b11) << 1) | (byte1 >> 7), 3));
		$('#CS_imm_5_6').html(addValue(((byte1 >> 5) & 0b11), 2));
		$('#CS_rs2').html(addValue(byte1 >> 2 & 0b111, 3));
		$('#CS_op').html(addValue(byte1 & 0b11, 2));

		// CB
		$('#CB_funct3').html(addValue(byte0 >> 5, 3));
		$('#CB_offset').html(addValue((byte0 >> 2), 3));
		$('#CB_rs1').html(addValue(((byte0 & 0b11) << 1) | (byte1 >> 7), 3));
		$('#CB_offset_2_6').html(addValue(((byte1 >> 2) & 0b11111), 5));
		$('#CB_op').html(addValue(byte1 & 0b11, 2));

		// CJ
		$('#CJ_funct3').html(addValue(byte0 >> 5, 3));
		$('#CJ_jump_target').html(addValue(((byte0 & 0b11111) << 6) | (byte1 >> 2), 11));
		$('#CJ_op').html(addValue(byte1 & 0b11, 2));
	}

	function revertDecodeBytes() {
		let v = $('#decodeRiscVField').val();
		let s = "";
		for (x = v.length - 2; x >= 0; x -= 2) {
			s += v.substr(x, 2);
		}
		$('#decodeRiscVField').val(s);
	}

	function addValue(v, noOfBit) {
		return v.toString(16) + "<br>" + beAFuckingTable(pad(v.toString(2), noOfBit))
	}

	// $('#addInstructionModal').on('shown.bs.modal', function() {
	// 	//alert('show');
	// 	$('#code').focus();
	// });
	// $('#addInstructionModal').on('hidden.bs.modal', function() {
	// 	$('#code').val('');
	// });

	$(".clickToSelect tr").click(function() {
		var selected = $(this).hasClass("highlight");
		$(".table tr").removeClass("highlight");
		if (!selected)
			$(this).addClass("highlight");
	});

	var timeoutDisasm;

	function loadDisassemblerResult() {
		if (timeoutDisasm) {
			clearTimeout(timeoutDisasm);
			timeoutDisasm = null;
		}
		timeoutDisasm = setTimeout(loadDisassemblerResult_real, 300);
	}

	function loadDisassemblerResult_real() {
		$('#testDisassemblerResult').html('<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />');
		originalPageNo = $('#testDisassemblerPageDropdown').val();
		if (originalPageNo == null) {
			originalPageNo = 0;
		}
		$('#testDisassemblerPageDropdown').html('');
		var form_data = new FormData();
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/loadDisassemblerResult.php?action=count&CI_JOB_ID=' + $('#testDisassemblerCIJOBIDDropdown').val() + '&search=' + $('#testDisassemblerSearch').val() + '&distance=' + $('#testDisassemblerDistance').val() + '&type=' + $('#testDisassemblerType').val(),
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			async: true,
			success: function(data) {
				$('#testDisassemblerCount').html('Count ' + data);

				if (data == 0) {
					$('#testDisassemblerResult').html('Result has nothing');
					return;
				}
				for (x = 0; x < data / 500; x++) {
					$('#testDisassemblerPageDropdown').append("<option>" + x + "</option>");
				}
				if (originalPageNo < data / 500) {
					$('#testDisassemblerPageDropdown').val(originalPageNo);
				} else {
					$('#testDisassemblerPageDropdown').val(0);
				}

				pageNo = $('#testDisassemblerPageDropdown').val();

				var form_data = new FormData();
				$.ajax({
					url: '<?php echo plugin_dir_url(__FILE__) ?>/loadDisassemblerResult.php?action=table&CI_JOB_ID=' + $('#testDisassemblerCIJOBIDDropdown').val() + '&page=' + pageNo + '&pageSize=500&search=' + $('#testDisassemblerSearch').val() + '&distance=' + $('#testDisassemblerDistance').val() + '&type=' + $('#testDisassemblerType').val(),
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					async: false,
					success: function(data) {
						$('#testDisassemblerResult').html(data);
					}
				});
			}
		});
	}

	$(function() {
		//reloadTable();
		reloadJobsTable();
		reloadPipelineResultTable(1);
		// reloadFullTestTable();
		// reloadFullTestRecordTable();
		reloadPipelineSummaryTable();
		loadInstructionDropdown('RISC-V');
		decodeRiscV($('#decodeRiscVField').val());
		loadSimulationRecord($('#testSimulatorDropdown').val());
		loadDisassemblerResult_real();
	});
</script>