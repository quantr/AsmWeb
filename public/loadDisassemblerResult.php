<?php
require '../../../../wp-config.php';
$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
if (!$conn) {
	echo 'Could not connect: ' . mysqli_error($conn);
}
?>
<? if ($_GET['action']=='table'){ ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>CI_JOB_ID</th>
			<th>Type</th>
			<th>Row No</th>
			<th>Bytes</th>
			<th>Quantr</th>
			<th>Objdump</th>
			<th>Distance</th>
		</tr>
	</thead>
	<tbody>
		<?php
				if ($_GET['search']==null) {
					$sql = "select * from disasm where CI_JOB_ID=? and distance<=? and type like concat('%',?,'%') order by type, rowNo limit ?,?";
				}else{
					$sql = "select * from disasm where CI_JOB_ID=? and distance<=? and type like concat('%',?,'%') and (replace(replace(LOWER(bytes),'0x',''), ' ', '') like concat('%',?,'%') or LOWER(quantr) like concat('%',?,'%') or LOWER(objdump) like concat('%',?,'%')) order by type, rowNo limit ?,?";
				}
				$stmt = mysqli_prepare($conn, $sql);
				$p=$_GET['page'] * $_GET['pageSize'];
				if ($_GET['search']==null) {
					mysqli_stmt_bind_param($stmt, "sisii", $_GET['CI_JOB_ID'], $_GET['distance'], $_GET['type'], $p, $_GET['pageSize']);
				}else{
					mysqli_stmt_bind_param($stmt, "sissssii", $_GET['CI_JOB_ID'], $_GET['distance'], $_GET['type'], strtolower($_GET['search']), strtolower($_GET['search']), strtolower($_GET['search']), $p, $_GET['pageSize']);
				}
				mysqli_stmt_execute($stmt);
				$result = mysqli_stmt_get_result($stmt);
				$x = 1;
				while ($row = mysqli_fetch_assoc($result)) {
					$objdumpLines = explode("\n", $row['objdump']);
					$objdumpLines = array_map('trim', $objdumpLines);
					$objdumpLines = implode("\n", $objdumpLines);
				?>
		<tr>
			<td><?= $row['id'] ?>
			</td>
			<td class="disasmBorderLeft"><?= $row['CI_JOB_ID'] ?>
			</td>
			<td class="disasmBorderLeft"><?= $row['type'] ?>
			</td>
			<td class="disasmBorderLeft"><?= $row['rowNo'] ?>
			</td>
			<td class="disasmBorderLeft"><?= str_replace('0x','',$row['bytes']) ?>
			</td>
			<td class="disasmBorderLeft">
				<pre
					style="margin: 0px;"><?= trim($row['quantr']) ?></pre>
			</td>
			<td class="disasmBorderLeft">
				<pre style="margin: 0px;"><?= $objdumpLines ?></pre>
			</td>
			<td class="disasmBorderLeft"><?= $row['distance'] ?>
			</td>
		</tr>
		<?
				}
			?>
	</tbody>
</table>
<? } else if ($_GET['action']=='count'){ ?>
<?php
		if ($_GET['search']==null) {
			$sql = "select count(*) from disasm where CI_JOB_ID=? and distance<=? and type like concat('%',?,'%')";
		}else{
			$sql = "select count(*) from disasm where CI_JOB_ID=? and distance<=? and type like concat('%',?,'%') and (replace(replace(LOWER(bytes),'0x',''), ' ', '') like concat('%',?,'%') or LOWER(quantr) like concat('%',?,'%'))";
		}
		
		$stmt = mysqli_prepare($conn, $sql);
        if ($_GET['search']==null) {
            mysqli_stmt_bind_param($stmt, "sis", $_GET['CI_JOB_ID'], $_GET['distance'], $_GET['type']);
        }else{
			mysqli_stmt_bind_param($stmt, "sissss", $_GET['CI_JOB_ID'], $_GET['distance'], $_GET['type'], strtolower($_GET['search']), strtolower($_GET['search']), strtolower($_GET['search']));
		}
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		$row = mysqli_fetch_assoc($result);
		echo $row['count(*)'];
	?>
<? } ?>