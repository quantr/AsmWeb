<div class="container-fluid">
	<table class="table table-striped " id="summaryTable">
		<thead class="thead-light">
			<tr>
				<th>Instruction</th>
				<th>Total</th>
				<th>Correct</th>
				<th>Wrong</th>
			</tr>
		</thead>
		<?php
		require '../../../../wp-config.php';
		$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
		if (!$conn) {
			echo 'Could not connect: ' . mysqli_error($conn);
		}
		if ($_GET['arch'] == 'IA32') {
			$sql = "select distinct lower(SUBSTRING_INDEX(test.code, ' ', 1)) as instruction, IFNULL(t.c,0) as total, IFNULL(anyCorrect.c,0) as anyCorrect, IFNULL(correct.c,0) as correct, IFNULL(mis.c,0) as misEncode, IFNULL(w.c,0) as wrong, IFNULL(useless.c, 0) as useless, (IFNULL(correct.c,0) + IFNULL(mis.c,0) + IFNULL(w.c,0) + IFNULL(useless.c,0)) as sum
			from test
			left join nasm
			on test.code=nasm.code
			left join
			(
				select distinct SUBSTRING_INDEX(test.code, ' ', 1) as instruction, count(*) as c
				from test
				where CI_JOB_ID=?
				group by instruction
			) as t on SUBSTRING_INDEX(test.code, ' ', 1) = t.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test.code, ' ', 1) as instruction, count(*) as c
				from test
				left join nasm
				on test.code=nasm.code
				where CI_JOB_ID=?
				and (nasm16 is not null or nasm32 is not null or nasm64 is not null) and (nasm16<=>quantr16 and nasm32<=>quantr32 and nasm64<=>quantr64) is true
				group by instruction
			) as correct on SUBSTRING_INDEX(test.code, ' ', 1) = correct.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test.code, ' ', 1) as instruction, count(*) as c
				from test
				left join nasm
				on test.code=nasm.code
				where CI_JOB_ID=?
				and (nasm16 is not null or nasm32 is not null or nasm64 is not null) and (nasm16<=>quantr16 or nasm32<=>quantr32 or nasm64<=>quantr64) is true
				group by instruction
			) as anyCorrect on SUBSTRING_INDEX(test.code, ' ', 1) = anyCorrect.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test.code, ' ', 1) as instruction, count(*) as c
				from test
				left join nasm
				on test.code=nasm.code
				where CI_JOB_ID=?
				and ((nasm16 is null and not quantr16 is null) or (nasm32 is null and not quantr32 is null) or (nasm64 is null and not quantr64 is null))
				group by instruction
			) as mis on SUBSTRING_INDEX(test.code, ' ', 1) = mis.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test.code, ' ', 1) as instruction, count(*) as c
				from test
				left join nasm
				on test.code=nasm.code
				where CI_JOB_ID=?
				and ((not quantr16<=>nasm16 and (nasm16Error is null or nasm16Error='' or nasm16Error like '%warning%')) or (not quantr32<=>nasm32 and (nasm32Error is null or nasm32Error='' or nasm32Error like '%warning%')) or (not quantr64<=>nasm64 and (nasm64Error is null or nasm64Error='' or nasm64Error like '%warning%')))
				group by instruction
			) as w on SUBSTRING_INDEX(test.code, ' ', 1) = w.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test.code, ' ', 1) as instruction, count(*) as c
				from test
				left join nasm
				on test.code=nasm.code
				where CI_JOB_ID=?
				and nasm16 is null and nasm32 is null and nasm64 is null and quantr16 is null and quantr32 is null and quantr64 is null
				group by instruction
			) as useless on SUBSTRING_INDEX(test.code, ' ', 1) = useless.instruction
			order by instruction";
			// echo $_GET['CI_JOB_ID']."<br>";
			// echo $sql;
			// die;
			$stmt = mysqli_prepare($conn, $sql);

			mysqli_stmt_bind_param($stmt, "ssssss", $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID']);
			mysqli_stmt_execute($stmt);
			// echo mysqli_stmt_error($stmt);
			$result = mysqli_stmt_get_result($stmt);
			$x = 0;
			while ($row = mysqli_fetch_assoc($result)) {
				if ($row['anyCorrect'] == 0 && $row['total'] == $row['misEncode'] + $row['useless']) {
					$wrongTestCases = '<span style="color: red;"> All test cases are wrong !!!</span>';
				} else {
					$wrongTestCases = null;
				}
				if ($_GET['showErrorOnly'] == 'true' && ($row['wrong'] == 0 && $wrongTestCases == null)) {
					continue;
				}
				echo "<tr " . ($row['wrong'] > 0 ? "style=\"background-color: #fff3f3\"" : "") . ">";
				echo "	<td style=\"cursor: pointer;\" onclick=\"jump('" . $row['instruction'] . "','" . $_GET['arch'] . "');\">" . $row['instruction'] . $wrongTestCases . "</td>";
				echo "	<td>" . $row['total'] . "</td>";
				echo "	<td>" . $row['correct'] . "</td>";
				echo "	<td>" . $row['misEncode'] . "</td>";
				echo "	<td>" . $row['wrong'] . "</td>";
				echo "	<td>" . $row['useless'] . "</td>";
				echo "</tr>\n";
				$x++;
			}
		} else if ($_GET['arch'] == 'RISC-V') {
			$sql = "select distinct lower(SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1)) as instruction, IFNULL(on9.c,0) as on9, IFNULL(t.c,0) as total, IFNULL(correct.c,0) as correct, IFNULL(w.c,0) as wrong, (IFNULL(correct.c,0) + IFNULL(w.c,0)) as sum
			from test_riscv
			left join riscv_gas
			on test_riscv.quantrCode=riscv_gas.quantrCode
			left join
			(
				select distinct SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) as instruction, count(*) as c
				from test_riscv
				where CI_JOB_ID=" . $_GET['CI_JOB_ID'] . "
				group by instruction
			) as t on SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) = t.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) as instruction, count(*) as c
				from test_riscv
				left join riscv_gas
				on test_riscv.quantrCode=riscv_gas.quantrCode
				where CI_JOB_ID=" . $_GET['CI_JOB_ID'] . "
				and quantr32 is null
				and quantr64 is null
				and gas32 is null
				and gas64 is null
				group by instruction
			) as on9 on SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) = on9.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) as instruction, count(*) as c
				from test_riscv
				left join riscv_gas
				on test_riscv.quantrCode=riscv_gas.quantrCode
				where CI_JOB_ID=" . $_GET['CI_JOB_ID'] . "
				and (quantr32<=>substring(gas32,1,19))
				and (quantr64<=>substring(gas64,1,19))
				group by instruction
			) as correct on SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) = correct.instruction
			left join
			(
				select distinct SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) as instruction, count(*) as c
				from test_riscv
				left join riscv_gas
				on test_riscv.quantrCode=riscv_gas.quantrCode
				where CI_JOB_ID=" . $_GET['CI_JOB_ID'] . "
				and (not quantr32<=>substring(gas32,1,19) or not quantr64<=>substring(gas64,1,19))
				group by instruction
			) as w on SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) = w.instruction
			where CI_JOB_ID=".$_GET['CI_JOB_ID']."
			order by instruction";
			// echo $_GET['CI_JOB_ID']."<br>";
			// echo "<pre>";
			// echo $sql;
			// echo "</pre>";
			// die;
			$stmt = mysqli_prepare($conn, $sql);

			//mysqli_stmt_bind_param($stmt, "ssssss", $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID'], $_GET['CI_JOB_ID']);
			mysqli_stmt_execute($stmt);
			echo mysqli_stmt_error($stmt);
			$result = mysqli_stmt_get_result($stmt);
			$x = 0;
			while ($row = mysqli_fetch_assoc($result)) {
				if ($row['total'] == $row['on9']) {
					$wrongTestCases = '<span style="color: red;"> All test cases are on9, because none of them can compile by gas !!!</span>';
				} else if ($row['wrong'] == $row['sum']) {
					$wrongTestCases = '<span style="color: red;"> All test cases are wrong !!!</span>';
				} else {
					$wrongTestCases = null;
				}
				if ($_GET['showErrorOnly'] == 'true' && $row['wrong'] == 0) {
					continue;
				}
				echo "<tr " . ($row['wrong'] > 0 ? "style=\"background-color: #fff3f3\"" : "") . ">";
				echo "	<td style=\"cursor: pointer;\" onclick=\"jump('" . $row['instruction'] . "','" . $_GET['arch'] . "');\">";
				if ($row['total'] != $row['on9'] && $row['wrong'] == 0) {
					echo '<span class="ci-status ci-success" style="width: 70px; display: inline-block; text-align: center; margin-right: 30px; padding:0px;">Success</span>&nbsp;';
				} else {
					echo '<span class="ci-status ci-failed" style="width: 70px; display: inline-block; text-align: center; margin-right: 30px; padding:0px;">Failed</span>&nbsp;';
				}
				echo $row['instruction'] . $wrongTestCases;
				echo "  </td>";
				echo "	<td>" . $row['total']  . "</td>";
				echo "	<td>" . $row['correct'] . "</td>";
				echo "	<td>" . $row['wrong'] . "</td>";
				echo "</tr>\n";
				$x++;
			}
		}
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
		?>
	</table>
	<br />
	No or row = <?= $x ?>
</div>
<script>
	function jump(instruction, arch) {
		shouldRefresh = false;
		$('#pipelineResultTab').click();
		$("#arch2[value='" + arch + "']").prop("checked", true);
		loadInstructionDropdown(arch);
		$('#pipelineResultInstructionDropdown').val(instruction);
		shouldRefresh = true;
		$("#pipelineResultInstructionDropdown").trigger("change");
	}
</script>