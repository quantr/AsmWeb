<?
require '../../../../wp-config.php';

$currentUser = wp_get_current_user();
$username=$currentUser->user_login;

global $wpdb;
$results = $wpdb->get_results("SELECT * FROM ".nasm_ia32_instruction." order by code limit 0,100;", OBJECT);
//var_dump($results);
foreach ($results as &$row) {
?>
	<tr>
		<td class="verticalSeparator1"><?= $row->createDate ?></td>
		<td class="verticalSeparator1"><?= $row->code ?></td>
		<td id="bit16_<?= $row->id ?>"><?= $row->bits16 ?></td>
		<td id="bit32_<?= $row->id ?>"><?= $row->bits32 ?></td>
		<td id="bit64_<?= $row->id ?>" class="verticalSeparator1"><?= $row->bits64 ?></td>
		<td id="bit16_quantr_assembler_<?= $row->id ?>"><?= $row->quantrBits16 ?></td>
		<td id="bit32_quantr_assembler_<?= $row->id ?>"><?= $row->quantrBits32 ?></td>
		<td id="bit64_quantr_assembler_<?= $row->id ?>" class="verticalSeparator1"><?= $row->quantrBits64 ?></td>
		<td id="bit64_pass_<?= $row->id ?>"><?= $row->pass==0?"no":"yes" ?></td>
		<td id="bit64_passDate_<?= $row->id ?>" class="verticalSeparator1"><?= $row->passDate ?></td>
		<td>
			<? if ($username=='admin'){ ?>
				<button type="button" class="btn btn-primary btn-sm compileButton" onclick="execNasm(<?= $row->id ?>);"><i class="fa fa-th"></i> Compile</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="if (confirm('sure to clear ?')){clearInstruction(<?= $row->id ?>);}"><i class="fa fa-eraser"></i> Clear</button>
				<button type="button" class="btn btn-danger btn-sm clearButton" style="display: none;" onclick="clearInstruction(<?= $row->id ?>);"><i class="fa fa-eraser"></i> Clear</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="if (confirm('sure to delete ?')){deleteInstruction(<?= $row->id ?>);}"><i class="fa fa-trash-o"></i> Del</button>
			<? } ?>
		</td>
	</tr>
<? } ?>
