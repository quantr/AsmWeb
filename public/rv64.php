<style>
	.tag {
		background-color: #a17fc9;
		color: white;
		border-radius: 20px;
		padding: 10px 20px;
		margin: 5px;
		line-height: 60px;
	}
</style>
<script>
	function extract(v, start, end) {
		n = BigInt(v);
		n = n >> BigInt(start);
		n = n & BigInt(Math.pow(2, end - start + 1) - 1);
		return n;
	}
	$(function() {
		$('#satp').keyup(function() {
			v = $('#satp').val();
			$('#satpDiv').html(
				`
				<div class="row">
				<div class="col">PPN</div><div class="col">0x` + (extract(v, 0, 43) << 12n).toString(16) + `</div>
				<div class="col">Mode</div><div class="col">0x` + extract(v, 60, 63).toString(16) + `</div>
				</div>
				`
			);
		})
		$('#satp').trigger('keyup');

		$('#va').keyup(function() {
			v = $('#va').val();
			$('#vaDiv').html(
				`
				<div class="row">
				<div class="col">PPN[2]</div><div class="col">0x` + extract(v, 30, 55).toString(16) + `</div>
				<div class="col">PPN[1]</div><div class="col">0x` + extract(v, 21, 29).toString(16) + `</div>
				<div class="col">PPN[0]</div><div class="col">0x` + extract(v, 13, 20).toString(16) + `</div>
				<div class="col">Offset</div><div class="col">0x` + extract(v, 0, 12).toString(16) + `</div>
				</div>
				`
			);
		})
		$('#va').trigger('keyup');

		for (x = 1; x <= 3; x++) {
			let z = x;
			$('#pageEntry' + z).keyup(function() {
				v = $('#pageEntry' + z).val();
				$('#pageEntryDiv' + z).html(
					`
				<div class="row">
				<div class="col">PPN</div><div class="col">0x` + (extract(v, 10, 53) << 12n).toString(16) + `</div>
				<div class="col">RSW</div><div class="col">0x` + extract(v, 8, 9).toString(16) + `</div>
				<div class="col">Attr</div><div class="col">0x` + extract(v, 0, 7).toString(2) + `</div>
				</div>
				`
				);
			})
			$('#pageEntry' + z).trigger('keyup');
		}

		splitByte();
	});

	function splitByte() {
		let index = 0;
		let splitByteValue = $('#splitByteValue').val();
		console.log(splitByteValue);
		$('#splitByteDiv').html('');
		for (v of $('#splitByte').val().split(',').reverse()) {
			v = parseInt(v);
			val = extract(splitByteValue, index, index + v - 1);
			console.log(v + ' = ' + index + ' , ' + (index + v - 1) + ' = ' + val);
			index += v;
			$('#splitByteDiv').html('<span class="tag">' + val.toString(16) + '</span>' + $('#splitByteDiv').html());
		}
	}
</script>
<style>
	label {
		color: #a17fc9;
		font-weight: bold;
	}

	.form-group {
		box-shadow: 0px 0px 4px 2px rgb(0 0 0 / 8%);
		padding: 20px;
	}
</style>
<div class="container-fluid mt-3">
	<div class="row">
		<div class="col-5">
			<div class="form-group">
				<label for="satp">SATP</label>
				<input type="text" class="form-control" name="satp" id="satp" aria-describedby="satp" placeholder="" value="0x8000000000080001" />
				<div id="satpDiv"></div>
			</div>
		</div>
		<div class="col-7">
			<div class="form-group">
				<label for="va">Virtual Address</label>
				<input type="text" class="form-control" name="va" id="va" aria-describedby="va" placeholder="" value="0x80000024" />
				<div id="vaDiv"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="form-group">
				<label for="pageEntry1">Page Entry 1st</label>
				<input type="text" class="form-control" name="pageEntry1" id="pageEntry1" aria-describedby="pageEntry1" placeholder="" value="0x2000080f" />
				<div id="pageEntryDiv1"></div>
			</div>
		</div>
		<div class="col">
			<div class="form-group">
				<label for="pageEntry2">Page Entry 2nd</label>
				<input type="text" class="form-control" name="pageEntry2" id="pageEntry2" aria-describedby="pageEntry2" placeholder="" value="0x2000080f" />
				<div id="pageEntryDiv2"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="form-group">
				<label for="pageEntry3">Page Entry 3rd</label>
				<input type="text" class="form-control" name="pageEntry3" id="pageEntry3" aria-describedby="pageEntry3" placeholder="" value="0x2000080f" />
				<div id="pageEntryDiv3"></div>
			</div>
		</div>
		<div class="col">
			<div class="form-group">
				<label for="pageEntry3">Split Byte</label>
				<input type="text" class="form-control mb-2" name="splitByte" id="splitByte" aria-describedby="pageEntry3" placeholder="" value="9,9,9,12" onKeyUp="splitByte();" />
				<input type="text" class="form-control" name="splitByteValue" id="splitByteValue" aria-describedby="pageEntry3" placeholder="" value="0x8000080f" onKeyUp="splitByte();" />
				<div id="splitByteDiv"></div>
			</div>
		</div>
	</div>
</div>