<?
require '../../../../wp-config.php';

global $wpdb;

if ($_GET['type']=='pass'){
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".nasm_ia32_instruction." where id=%d;", $_GET['id']), OBJECT);
	if ($results[0]->bits16==$results[0]->quantrBits16 && $results[0]->bits32==$results[0]->quantrBits32 && $results[0]->bits64==$results[0]->quantrBits64){
		$_GET['data']=1;
	}else{
		$_GET['data']=0;
	}
	$wpdb->update(
		nasm_ia32_instruction,
		array(
			pass => $_GET['data'],
			passDate => current_time('mysql')
		),
		array('id'=> $_GET['id'])
	);
}else if ($_GET['type']=='quantrAssembler'){
	if ($_GET['bits']==16){
		$column='quantrBits16';
	}else if ($_GET['bits']==32){
		$column='quantrBits32';
	}else if ($_GET['bits']==64){
		$column='quantrBits64';
	}

	$wpdb->update(
		nasm_ia32_instruction,
		array(
			$column => $_GET['data']
		),
		array('id'=> $_GET['id'])
	);
}else{
	if ($_GET['bits']==16){
		$column='bits16';
	}else if ($_GET['bits']==32){
		$column='bits32';
	}else if ($_GET['bits']==64){
		$column='bits64';
	}

	$wpdb->update(
		nasm_ia32_instruction,
		array(
			$column => $_GET['data']
		),
		array('id'=> $_GET['id'])
	);
}

echo "ok";
?>
