<?php
if (is_user_logged_in() == null) {
?>
	<div style="text-align: center; margin: 50px 0px;">Please <a href="https://www.quantr.foundation/wp-login.php?redirect_to=<?= get_permalink() ?>">login</a></div>
<?
	die;
}
$currentUser = wp_get_current_user();
$username = $currentUser->user_login;
?>
<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__) ?>/fontawesome-free-5.10.1-web/css/all.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<style>
	#qfLogo {
		width: 30px;
	}

	#cpuRecorderTable td {
		white-space: nowrap;
	}

	#simulatorTable td {
		white-space: nowrap;
	}

	.browseButton {
		border: 2px solid #8c7bb1;
		margin-top: auto;
		margin-right: 5px;
		margin-bottom: auto;
		padding: 5px;
		border-radius: 5px;
	}

	.hiddenText {
		width: 0px;
		height: 0px;
		overflow: hidden;
		display: inline-block;
	}

	.myFileInput {
		color: transparent;
	}

	.myFileInput {
		height: 38px;
		width: 105px;
	}

	.myFileInput::-webkit-file-upload-button {
		visibility: hidden;
	}

	.myFileInput::before {
		content: 'dump.json';
		color: black;
		display: inline-block;
		outline: none;
		white-space: nowrap;
		-webkit-user-select: none;
		cursor: pointer;

		color: #fff !important;
		background-color: #8c7bb1 !important;
		border: 1px solid #653d8a !important;
		padding: .375rem .75rem;
		border-radius: .25rem;
	}

	.myFileInput:active::before {
		background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
	}

	.hoverTR {
		background-color: #fbe7fe !important;
	}
</style>
<script>
	$(function() {
		loadSimulationRecord($('#testSimulatorDropdown').val());
	});

	function loadSimulationRecord(guid) {
		$('#cpuRecorderTable').html(`
			<tr>
				<td style="text-align: center;">
					<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
				</td>
			</tr>
		`);
		$.get(
			'<?php echo plugin_dir_url(__FILE__) ?>/reloadCpuRecorderTableH2.php?guid=' + guid + '&mode=' + $('#disasmMode').prop('checked') + '&page=' + $('#page').val() + '&pageSize=' + $('#pageSize').val(),
			function(data) {
				$('#cpuRecorderTable').html(data);
				compactMode();

				$('#cpuRecorderTable').find('tr').hover(function() {
					let rowNo = $(this).index() + 1;
					$('#simulatorTable tbody tr td').removeClass('hoverTR');
					$('#simulatorTable tbody tr:nth-child(' + rowNo + ') td').addClass('hoverTR');
					$('#cpuRecorderTable tbody tr td').removeClass('hoverTR');
					$('#cpuRecorderTable tbody tr:nth-child(' + rowNo + ') td').addClass('hoverTR');
				})
			});
		loadSimulatorBinary(guid);
	}

	function loadSimulatorBinary(guid) {
		$('#simulatorTable').html(`
			<tr>
				<td style="text-align: center;">
				<img src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
				</td>
			</tr>
		`);
		setTimeout(function() {
			var file_data = $('#fileUploaderSimulatorBinary').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			form_data.append('page', $('#page').val());
			$.ajax({
				url: '<?php echo plugin_dir_url(__FILE__) ?>/uploadSimulatorBinary.php?mode=' + $('#disasmMode').prop('checked') + '&guid=' + guid + '&page=' + $('#page').val() + '&pageSize=' + $('#pageSize').val(),
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					$('#simulatorTable').html(data);

					$('#simulatorTable').find('tr').hover(function() {
						let rowNo = $(this).index() + 1;
						$('#simulatorTable tbody tr td').removeClass('hoverTR');
						$('#simulatorTable tbody tr:nth-child(' + rowNo + ') td').addClass('hoverTR');
						$('#cpuRecorderTable tbody tr td').removeClass('hoverTR');
						$('#cpuRecorderTable tbody tr:nth-child(' + rowNo + ') td').addClass('hoverTR');
					})
					$('#fileUploaderSimulatorBinary').val('');
					compactMode();
				}
			});
		}, 100);
	}

	function compactMode() {
		let v = $('#compact').prop('checked');
		if (v) {
			$('#twoTable td').addClass('py-0');
			// $('#twoTable td').removeClass('py-0');
			paddingOri0 = $('#simulatorTable td').css('padding');
			// $('#twoTable td').css('padding', '0px');
		} else {
			// $('#twoTable td').addClass('px-1');
			$('#twoTable td').removeClass('py-0');
			// $('#twoTable td').css('padding', paddingOri0);
		}
	}
</script>
<style>
	#simulator h1 {
		color: #a17fc9;
		font-size: 26px;
		margin: 0px;
	}
</style>
<div id="simulator" class="container-fluid p-0">
	<div class="row">
		<div class="col-12 form-inline justify-content-center mt-2">
			<h1>Simulator workbench</h1>&nbsp;&nbsp;
			<?
				date_default_timezone_set('Asia/HongKong');
				$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';
				// echo "$jdk_home/bin/java -jar " . plugin_dir_path(__FILE__) . "/H2ToJson*.jar -u sa -p fuck1234shit -j jdbc:h2:tcp://quantr.hk//root/h2_qemu_recorder/qemuRecord -s \"select distinct guid,date from qemurecorder_" . $username . " order by date desc\"";
			?>
			<select id="testSimulatorDropdown" class="form-control" style="width: 320px;" onchange="loadSimulationRecord($('#testSimulatorDropdown').val()); loadSimulatorBinary($('#testSimulatorDropdown').val());">
				<?
				$str = json_decode(exec("$jdk_home/bin/java -jar " . plugin_dir_path(__FILE__) . "/H2ToJson*.jar -u sa -p fuck1234shit -j jdbc:h2:tcp://quantr.hk//root/h2_qemu_recorder/qemuRecord -s \"select distinct guid,date from qemurecorder_" . $username . " order by date desc\""));

				for ($x = 0; $x < count($str); $x++) {
					$unix_timestamp = $str[$x]->DATE / 1000;
					$datetime = new DateTime("@$unix_timestamp");
					$date_time_format = $datetime->format('Y-m-d H:i:s');
					$time_zone_from = "UTC";
					$time_zone_to = 'Asia/Hong_Kong';
					$display_date = new DateTime($date_time_format, new DateTimeZone($time_zone_from));
					$display_date->setTimezone(new DateTimeZone($time_zone_to));
				?>
					<option value='<?= $str[$x]->GUID ?>' <? if ($_SESSION['guid'] == $str[$x]->GUID) { ?>selected<? } ?>><?= $str[$x]->GUID ?> - <?= $display_date->format('Y-m-d') ?></option>
				<? } ?>
			</select>
			<input type="checkbox" id="disasmMode" class="form-check-input ml-2" checked onchange="loadSimulationRecord($('#testSimulatorDropdown').val()); loadSimulatorBinary($('#testSimulatorDropdown').val());" />
			<div style="width: 120px;">
				<label for="disasmMode" style="margin-bottom: 0px;">Simplified Table</label>
			</div>
			<input type="file" id="fileUploaderSimulatorBinary" class="myFileInput" onchange="loadSimulatorBinary($('#testSimulatorDropdown').val());" />
			<!-- <label for="fileUploaderSimulatorBinary" class="browseButton">Browse</label> -->
			<button type="button" onclick="loadSimulatorBinary($('#testSimulatorDropdown').val());" size="10" class="btn btn-primary">Load</button>
			&nbsp;
			<select class="form-control" id="page" onchange="loadSimulationRecord($('#testSimulatorDropdown').val());">
				<? for ($x = 0; $x < 100; $x++) { ?>
					<option value="<?= $x ?>" <? if ($_SESSION['page'] == $x) { ?>selected<? } ?>><?= $x ?></option>
				<? } ?>
			</select>
			&nbsp;
			<select class="form-control" id="pageSize" onchange="loadSimulationRecord($('#testSimulatorDropdown').val());">
				<option>100</option>
				<option>500</option>
				<option>1000</option>
				<option>2000</option>
			</select>
			&nbsp;
			<i class="fas fa-check-circle" style="color: #00cc00;"></i>&nbsp;<span id="simulatorCorrect">0</span>
			&nbsp;
			<i class="fas fa-times-circle" style="color: #cc0000;"></i>&nbsp;<span id="simulatorWrong">0</span>
			&nbsp;
			<button class="btn btn-primary" onclick="$('#cpuRecorderTableLeft').removeClass();$('#simulatorTableDiv').removeClass();$('#cpuRecorderTableLeft').addClass('col-9');$('#simulatorTableDiv').addClass('col-3');">9:3</button>&nbsp;
			<button class="btn btn-primary" onclick="$('#cpuRecorderTableLeft').removeClass();$('#simulatorTableDiv').removeClass();$('#cpuRecorderTableLeft').addClass('col-6');$('#simulatorTableDiv').addClass('col-6');">6:6</button>&nbsp;
			<button class="btn btn-primary" onclick="$('#cpuRecorderTableLeft').removeClass();$('#simulatorTableDiv').removeClass();$('#cpuRecorderTableLeft').addClass('col-4');$('#simulatorTableDiv').addClass('col-8');">4:8</button>&nbsp;
			<button class="btn btn-primary" onclick="$('#cpuRecorderTableLeft').removeClass();$('#simulatorTableDiv').removeClass();$('#cpuRecorderTableLeft').addClass('col-3');$('#simulatorTableDiv').addClass('col-9');">3:9</button>&nbsp;
			<input type="checkbox" id="compact" name="compact" checked class="form-check-input" value="normal" onclick="compactMode()" />
			<label for="compact">Compact</label>
		</div>
	</div>
	<div id="twoTable" class="row" style="margin-top: 10px;">
		<div class="col-6 p-0" id="cpuRecorderTableLeft" style="overflow: scroll;">
			<table id="cpuRecorderTable" class="table table-striped">
			</table>
		</div>
		<div class="col-6 p-0" id="simulatorTableDiv" style="overflow: scroll; border-left: 2px solid #d8d8d8;">
			<table id="simulatorTable" class="table table-striped">
			</table>
		</div>
	</div>
</div>